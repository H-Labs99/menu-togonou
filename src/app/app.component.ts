import { Component } from '@angular/core';

// import { Menu } from './menu';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'menu-togonou';
  // menu = new Menu("Akoume", "https://guillaumeinternoscia.files.wordpress.com/2013/07/p1030402.jpg", "Ici il n’est pas question de cervelles de singes ou de brochettes de chien, d’ailleurs la viande de chiens je trouve que ça sent mauvais et selon mes goûts c’est encore moins intéressant que la tête de mouton. Le rat par contre c’est pas si mal.");
}
