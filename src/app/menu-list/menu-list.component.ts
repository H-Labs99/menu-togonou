import { Component, OnInit } from '@angular/core';

import { Menu } from '../menu';
@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  public menus: Menu[] = [
    new Menu("Akoume", 'https://guillaumeinternoscia.files.wordpress.com/2013/07/p1030402.jpg', 'Ici il n’est pas question de cervelles de singes ou de brochettes de chien, d’ailleurs la viande de chiens je trouve que ça sent mauvais et selon mes goûts c’est encore moins intéressant que la tête de mouton. Le rat par contre c’est pas si mal.'),
    new Menu("Watchi", 'https://actu-togo.com/wp-content/uploads/2019/12/Ayimolou-13-500x500.jpg', 'Chez nous au Togo, on a pris pour habitude d’attribuer notre humeur de la journée à notre premier repas. Certains se sentent d’aplomb juste après le petit déjeuner, d’autres par contres n’ont tout juste plus envie de lever le petit doigt. A Kara, l’AYIMOLOU, qui est un plat fait à base de riz et de haricot préparé a rendu le petit déjeuner encore plus extra. Tellement ce repas rend la communauté heureuse. Pour vous, nous avons fait le TOP des tops des vendeuses d’AYIMOLOU de Kara à ne surtout pas rater. Tu goutes, tu restes !!'),
    new Menu("Salada", 'https://assets.afcdn.com/recipe/20161010/64090_w350h250c1cx3680cy2456.jpg', 'salade de ma tanti du quartier elle fait ca tres bien venez juste manger'),
    new Menu("Salade de boulghour au poulet harissa", 'https://recettes.de/images/blogs/chez-becky-et-liz/salade-de-boulghour-au-poulet-harissa.640x480.jpg', 'Ici, vous trouverez des recettes authentiques, traditionnelles écossaises, galloises, anglaises, irlandaises (la liste est bien garnie) et leurs petites histoires, mais également des plats contemporains, modernes, innovants de chefs britanniques, quelques idées de balades dans la campagne anglaise, au Pays de Galles, Écosse, Irlande du Nord, et des adresses en France')
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
